from Personaje import Medico, Artillero, Inteligencia, Francotirador


class Jugador:
    def __init__(self, oponente, equipo):
        self.oponente = None
        self.equipo = []
        self.turnos = True
        self.resultado_anterior = None
        self.turno_accion = 1
        # self.informe = informe

    def set_oponente(self, oponente1):
        self.oponente = oponente1
        self.crear_equipo()
        self.posicionar_equipo()

    def turno(self):

        self.turno_accion += 1
        accion_jugador = Jugador.get_accion_realizada(self)

        print(" --- INFORME ---")
        print(f"{accion_jugador}")

        print("\n")
        print("--- SITUACION DEL EQUIPO ---")
        for i in self.equipo:
            print(f"{i} esta en {i.posicion.upper()} con vida [{i.vida_actual}/{i.vida_maxima}]")
        print("\n")

        accion = self.realizar_accion()
        resultado = self.recibir_accion(accion)

        if "movido" in resultado:
            self.oponente.resultado_anterior = "Personaje Movido posicion oculta"
        elif "curado" in resultado:
            self.oponente.resultado_anterior = "Personaje curado posicion oculta"

        else:
            self.oponente.resultado_anterior = resultado

        if self.turno_accion % 3 == 0:
            for i in self.equipo:
                if i.enfriamiento_restante == 1:
                    i.enfriamiento_restante = 0
                else:
                    continue

        print("--- RESULTADO DE LA ACCION ---")
        print(self.informe(resultado))
        if Artillero.get_vida(self.oponente.equipo[1]) <= 0 and Francotirador.get_vida(self.oponente.equipo[3]) <= 0:
            return True

    def crear_equipo(self):
        Med = Medico(self, 1, 1, 0, 0, [])
        self.equipo.append(Med)

        Art = Artillero(self, 2, 1, 2, 0, [])
        self.equipo.append(Art)

        Int = Inteligencia(self, 2, 2, 2, 0, [])
        self.equipo.append(Int)

        Fra = Francotirador(self, 3, 3, 2, 0, [])
        self.equipo.append(Fra)

        return self.equipo

    def realizar_accion(self):

        print("""
 ---- ACCIONES ----
1: Mover (Medico)
2: Curar Personaje
3: Mover (Artillero)
4: Disparar en área (2x2).Daño 1.(Artillero)
5: Mover (Francotirador)
6: Disparar  una celda. Daño 3.(Francotirador)
7: Mover(Inteligencia)
8: Revelar a los enemigos enemigos en un área 2x2. (Inteligencia)
--ANOTACION--
Si el personaje esta muerto, no podra realizar sus acciones correspondientes
                """)
        accion_valida = True
        while True:
            try:
                accion = str(input("Selecciona la acción de este turno:"))
                if accion == "1" and (Medico.get_vida(self.equipo[0]) > 0):

                    return Medico.mover(self.equipo[0], self.equipo)

                elif accion == "2" and (Medico.get_vida(self.equipo[0]) > 0):
                    if self.equipo[0].enfriamiento_restante == 0:
                        self.equipo[0].enfriamiento_restante += 1
                        accion_valida = True
                        return Medico.curar(self.equipo[0], self.equipo)
                    else:
                        print("Todavia queda un turno para usar la habilidad")
                        accion_valida = False

                elif accion == "3" and (Artillero.get_vida(self.equipo[1]) > 0):

                    return Artillero.mover(self.equipo[1], self.equipo)

                elif accion == "4" and (Artillero.get_vida(self.equipo[1]) > 0):
                    if self.equipo[1].enfriamiento_restante == 0:

                        self.equipo[1].enfriamiento_restante += 1
                        return Artillero.art_hability(self.oponente.equipo[1],self.oponente.equipo)

                    else:
                        print("Todavia queda un turno para usar la habilidad")
                        accion_valida = False

                elif accion == "5" and (Francotirador.get_vida(self.equipo[3]) > 0):

                    return Francotirador.mover(self.equipo[3], self.equipo)

                elif accion == "6" and (Francotirador.get_vida(self.equipo[3]) > 0):
                    if self.equipo[3].enfriamiento_restante == 0:
                        self.equipo[3].enfriamiento_restante += 1
                        return Francotirador.habilidad(self.equipo[3], self.oponente.equipo)
                    else:
                        print("Todavia queda un turno para usar la habilidad")
                        accion_valida = False

                elif accion == "7" and (Inteligencia.get_vida(self.equipo[2]) > 0):

                    return Inteligencia.mover(self.equipo[2], self.equipo)

                elif accion == "8" and (Inteligencia.get_vida(self.equipo[2]) > 0):
                    if self.equipo[2].enfriamiento_restante == 0:
                        self.equipo[2].enfriamiento_restante += 1
                        return Inteligencia.intel_hability(self.equipo[2], self.oponente.equipo)
                    else:
                        print("Todavia queda un turno para usar la habilidad")
                        accion_valida = False
                else:
                    print("Jugador invalido, seleccione una accion valida")
                    continue
            except ValueError and IndexError:
                print("Valor invalido")

    def posicionar_equipo(self):
        print("Coloque a los Personajes en el campo de batalla")
        # pregunto al usuario en    que celda quiere posicionar el medico

        Medico.get_posicion(self.equipo[0], self.equipo)
        Artillero.get_posicion(self.equipo[1], self.equipo)
        Inteligencia.get_posicion(self.equipo[2], self.equipo)
        Francotirador.get_posicion(self.equipo[3], self.equipo)

        return self.equipo

    def recibir_accion(self, str):

        if str == None:
            return "La accion no ha tenido consecuencia "

        else:
            return f"{str}"

    def get_accion_realizada(self):
        return self.resultado_anterior

    def informe(self, resultado):

        return (f""" 
{resultado} 
            """)
