import utils


class Personaje:
    def __init__(self, vida_maxima, vida_actual, danyo, posicion, enfriamiento_restante, equipo):
        self.vida_maxima = 0
        self.vida_actual = vida_actual
        self.danyo = danyo
        self.posicion = posicion
        self.enfriamiento_restante = enfriamiento_restante
        self.equipo = equipo

    def get_posicion(self, equipo):
        while True:
            try:
                New_Pos = str(input(f"Indica la celda (A-D, 1-4. p.ej: B2) en la que posicionar al {self} :"))
                if len(New_Pos) <=2:
                    if utils.validar_celda(New_Pos, 4, 4):
                        if utils.comprobar_celda_disponible(New_Pos, equipo):
                            self.posicion = New_Pos
                            return self.posicion
                        else:
                            continue
                    else:
                        continue
                else:
                    print("Valor no valido")
                    continue


            except ValueError and IndexError:
                print("valor no valido")



    def mover(self, equipo):
        print(f"Vamos a mover al {self} posicion en {self.posicion.upper()}, el movimiento ha de ser (← ↑ → ↓).")
        while True:
            New_pos = str(input("Seleccione la nueva posicion:"))
            if utils.validar_celda(New_pos, 4, 4):
                if utils.comprobar_celda_disponible(New_pos, equipo):
                    if utils.validar_celda_contigua(self.posicion, New_pos):
                        self.posicion = New_pos
                        return f" {self} movido a {New_pos.upper()}"
                    else:
                        print("Posicion no contigua")
                        continue
                else:
                    continue

            else:
                continue

    def review_posicion(self):
        self.posicion.upper()
        return self.posicion.upper()

    def get_vida_max(self):
        return self.vida_maxima

    def get_vida(self):
        return self.vida_actual

    def hit(self, posicion_ataque):
        if self.vida_actual != 0:
            if self.posicion in posicion_ataque:
                self.vida_actual -= 1
            else:
                return None
            return self.vida_actual
        else:
            print("Jugador Muerto")
            return self.vida_actual

    def curar(self, equipo):
            pos = str(input("Seleccione celda para curar: "))
            for i in equipo:
                if pos == i.posicion:
                    if i.vida_actual > 0 :
                        if i.vida_actual != i.vida_maxima:
                            i.vida_actual += 1
                            return f"{i} curado at {pos.upper()} "
                        else:

                            print(f"{i} ya tiene la vida al maximo, no se puede curar")
                            continue


                    else:
                        print(f"{i} muerto")
                        break

                else:
                    continue


    def get_enfriamiento(self):
        self.enfriamiento_restante = 0
        return self.enfriamiento_restante


class Medico(Personaje):
    def __init__(self, vida_maxima, vida_actual, danyo, posicion, enfriamiento_restante, equipo):
        super().__init__(vida_maxima, vida_actual, danyo, posicion, enfriamiento_restante, equipo)
        self.vida_maxima = 1
        self.vida_actual = 1

    def __str__(self):
        return "Medico"

    def review_posicion(self):
        self.posicion.upper()
        return self.posicion.upper()

    def get_vida(self):
        return self.vida_actual

    def get_vida_max(self):
        return self.vida_maxima


class Artillero(Personaje):
    def __init__(self, vida_maxima, vida_actual, danyo, posicion, enfriamiento_restante, equipo):
        super().__init__(vida_maxima, vida_actual, danyo, posicion, enfriamiento_restante, equipo)
        self.vida_maxima = 2
        self.vida_actual = 2

    def __str__(self):
        return "Artillero"

    def get_vida_max(self):
        return self.vida_maxima

    def get_vida(self):
        return self.vida_actual

    def review_posicion(self):
        return self.posicion.upper()

    def art_hability(self,equipo):
        while True:
            try:
                attack = input("Seleccione una celda para atacar:")
                self.posicion_ataque = utils.positions_of_attack(attack)
                pos2 = {}
                resultado = ""
                for i in equipo:
                    if i.posicion in self.posicion_ataque:
                        i.hit(self.posicion_ataque)
                        pos2[i] = i.posicion
                    else:
                        continue
                if not pos2:
                    return None

                for player, pos in pos2.items():
                    resultado += f"{player} ha sido herido en  {pos.upper()}, vida restante: [{player.vida_actual}/{player.vida_maxima}] " "\n"
                return resultado


            except ValueError and IndexError:
                print("Valor invalido")


class Francotirador(Personaje):
    def __init__(self, vida_maxima, vida_actual, danyo, posicion, enfriamiento_restante, equipo):
        super().__init__(vida_maxima, vida_actual, danyo, posicion, enfriamiento_restante, equipo)
        self.vida_actual = int(3)
        self.vida_maxima = int(3)

    def __str__(self):
        return "Francotirador"

    def habilidad(self, equipo):
            impacto = False
            pos = str(input("Seleccione celda para disparar: "))
            for i in equipo:
                if i.posicion == pos:
                    i.vida_actual = 0
                    impacto = True
                    return f"{i} dado en {i.posicion.upper()}, vida restante [{i.vida_actual}/{i.vida_maxima}]"
                else:
                    continue
            if not impacto:
                return None

    def get_vida_max(self):
        return self.vida_maxima

    def get_vida(self):
        return self.vida_actual

    def review_posicion(self):
        return self.posicion.upper()


class Inteligencia(Personaje):
    def __init__(self, vida_maxima, vida_actual, danyo, posicion, enfriamiento_restante, equipo):
        super().__init__(vida_maxima, vida_actual, danyo, posicion, enfriamiento_restante, equipo)

        self.vida_maxima = 2
        self.vida_actual = 2

    def __str__(self):
        return "Inteligencia"

    def get_vida_max(self):
        return self.vida_maxima

    def get_vida(self):
        return self.vida_actual

    def review_posicion(self):
        return self.posicion.upper()

    def intel_hability(self, equipo):
        while True:
            try:
                vision = {}
                vision_str = ""
                uav = Inteligencia.UAV(self)
                for i in equipo:
                    if i.posicion in uav:
                        vision[i] = i.posicion
                        continue
                if not vision:
                    return None
                for personaje, posicion in vision.items():
                    vision_str += f"{personaje} ha sido avistado en  {posicion.upper()} \n"
                return vision_str

            except ValueError and IndexError:
                print("Valor invalido")

    def UAV(self):
        map = input("Seleccione una celda para atacar:")
        self.posicion_ataque = utils.positions_of_attack(map)
        return self.posicion_ataque
