import socket
import threading
import random
import time
import jugador
import pickle

# Create server socket
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(("127.0.0.1", 5001))
print("Esperando a jugadores...")
server.listen()

clients = []  # connected clients
lobby = []  # clients waiting in lobby


class Client:
    def __init__(self, username, socket):
        self.username = username
        self.socket = socket






def start_game(client1, client2):
    client1.socket.send(client2.username.encode())
    client2.socket.send(client1.username.encode())

    # Receive and send player dictionaries
    player1_data = pickle.loads(client1.socket.recv(1024))
    player2_data = pickle.loads(client2.socket.recv(1024))

    # Send player dictionaries to their respective opponents
    client1.socket.send(pickle.dumps(player2_data))
    client2.socket.send(pickle.dumps(player1_data))

    print("Player 1 data:", player1_data["equipo"])
    print("Player 2 data:", player2_data["equipo"])


while True:
    client_socket, address = server.accept()
    data = client_socket.recv(1024).decode()
    PLAYER = Client(data, client_socket)
    lobby.append(PLAYER)
    if len(lobby) >= 2:
        client1 = lobby[0]
        client2 = lobby[1]
        threading.Thread(target=start_game, args=(client1, client2)).start()
        print("Jugadores encontrados", client1.username, ",", client2.username)
        print("Empezando partida")
        lobby.clear()


