from jugador import Jugador


def limpiar_terminal():
    print(chr(27) + "[2J")


def validar_celda(celda, max_col, max_row):
    celda1 = celda.upper()
    col = ["A", "B", "C", "D"]
    fil = [1, 2, 3, 4]

    if celda1[0] not in "ABCD" or int(celda1[1]) >= 5:
        print("Posicion fuera del tablero")
        return False
    else:
        return True


# Para comprobar si una celda "B5" está dentro es una posición válida del
# tablero que comprende entre A1 y (max_col, max_row)
def comprobar_celda_disponible(celda, equipo) -> bool:
    Pos_list= []
    for i in equipo:
        Pos_list.append(i.posicion)
    if celda in Pos_list:
        print("Esta celda ya esta siendo ocuapada")
    else:
        return True




def validar_celda_contigua(celda1, celda2) -> bool:  # Para comprobar si un miembro del equipo ya ocupa una celda dada
    celda2 = celda2.upper()
    celda1 = celda1.upper()
    col = ["A", "B", "C", "D"]
    fil = [1, 2, 3, 4]

    col1 = celda1[0]
    fil1 = int(celda1[1])

    col2 = celda2[0]
    fil2 = int(celda2[1])

    # Check column difference
    col_diff = abs(col.index(col1) - col.index(col2))
    misma_col = col_diff <= 1
    if not misma_col:
        return False
    elif (misma_col == True and (col1 == col2)) or (fil1 == fil2):
        # Check row difference
        fil_diff = abs(fil1 - fil2)
        misma_fil = fil_diff <= 1
    else:
        return False

        # Return boolean result
    return misma_col and misma_fil


def positions_of_attack(celda):
    col = ["a", "b", "c", "d"]
    fil = [1, 2, 3, 4]

    # Obtener columna y fila de la celda
    col_celda = celda[0]
    fil_celda = int(celda[1])

    # Posiciones de ataque
    posiciones = []

    posiciones.append(celda)
    # Arriba
    if 1 <= fil_celda <= 5:
        posiciones.append(col_celda + str(fil_celda + 1))

    # Derecha Arriba
    if col_celda == "d":
        posiciones.append(col[col.index(col_celda) - 1] + str(fil_celda + 1))
        posiciones.append(col[col.index(col_celda) - 1] + str(fil_celda))
    else:

        posiciones.append(col[col.index(col_celda) + 1] + str(fil_celda + 1))
        posiciones.append(col[col.index(col_celda) + 1] + str(fil_celda))

    # Derecha arriba

    return posiciones
